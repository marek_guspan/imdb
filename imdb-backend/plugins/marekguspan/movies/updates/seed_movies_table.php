<?php namespace MarekGuspan\Movies\Updates;

use Seeder;
use MarekGuspan\Movies\Models\Movie;

class SeedMoviesTable extends Seeder
{
    public function run()
    {
        Movie::create([
            'name'                  => 'It',
            'year'                  => 2017,
            'length'                => 135,
            'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
            'img'                   => 'https://cdn-static.denofgeek.com/sites/denofgeek/files/styles/main_wide/public/2019/09/it-chapter-one-promo.jpg?itok=p75tR20W'
        ]);
        Movie::create([
            'name'                  => 'Wolf of Wallstreet',
            'year'                  => '2013',
            'length'                => 180,
            'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
            'img'                   => 'https://static1.srcdn.com/wordpress/wp-content/uploads/2019/11/wolf-of-wall-street-2013-poster.jpg'
        ]);
        Movie::create([
            'name'                  => 'Interstellar',
            'year'                  => '2014',
            'length'                => 169,
            'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
            'img'                   => 'https://i.ytimg.com/vi/VNd_WIcgnYI/maxresdefault.jpg'
        ]);
        Movie::create([
            'name'                  => 'Inception',
            'year'                  => '2010',
            'length'                => 148,
            'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
            'img'                   => 'https://blabla1.eu/VideoBuff/wp-content/uploads/2018/12/Inception.jpg'
        ]);
        Movie::create([
            'name'                  => 'Joker',
            'year'                  => '2019',
            'length'                => 122,
            'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
            'img'                   => 'https://images.ladbible.com/thumbnail?type=jpeg&url=http://beta.ems.ladbiblegroup.com/s3/content/1d2200895d5ea6e07a43fefb87ac56c4.png&quality=70&width=720'
        ]);
        Movie::create([
            'name'                  => 'Friends with benefits',
            'year'                  => '2011',
            'length'                => 105,
            'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
            'img'                   => 'https://www.filmofilia.com/wp-content/uploads/2011/05/friends_with-benefits.jpg'
        ]);
        // Movie::create([
        //     'name'                  => 'The Hobbit: An Unexpected Journey',
        //     'year'                  => '2012',
        //     'length'                => 169,
        //     'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
        //     'img'                   => 'https://stcmicrosoft.azurewebsites.net/img/studenti/2018/glasnak-matej.jpg'
        // ]);
        // Movie::create([
        //     'name'                  => 'The Hobbit: The Desolation of Smaug',
        //     'year'                  => '2013',
        //     'length'                => 161,
        //     'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
        //     'img'                   => 'https://stcmicrosoft.azurewebsites.net/img/studenti/2018/glasnak-matej.jpg'
        // ]);
        // Movie::create([
        //     'name'                  => 'The Hobbit: The Battle of the Five Armies',
        //     'year'                  => '2014',
        //     'length'                => 144,
        //     'description'           => 'Some dumb description about this fucking movie, that is absolutely boring btw.',
        //     'img'                   => 'https://stcmicrosoft.azurewebsites.net/img/studenti/2018/glasnak-matej.jpg'
        // ]);
        // Movie::create([
        //     'name'                  => 'Wolf of Wallstreet',
        //     'year'                  => '2013',
        //     'genre'                 => 'Comedy',
        //     'length'                => 180,
        //     'has_watched'           => true,
        //     'director'              => 'Martin Scorsese'
        // ]);
        // Movie::create([
        //     'name'                  => 'Interstellar',
        //     'year'                  => '2014',
        //     'genre'                 => 'Sci-Fi',
        //     'length'                => 169,
        //     'has_watched'           => true,
        //     'director'              => 'Christopher Nolan'
        // ]);
        // Movie::create([
        //     'name'                  => 'Inception',
        //     'year'                  => '2010',
        //     'genre'                 => 'Sci-Fi',
        //     'length'                => 148,
        //     'has_watched'           => false,
        //     'director'              => 'Christopher Nolan'
        // ]);
        // Movie::create([
        //     'name'                  => 'Joker',
        //     'year'                  => '2019',
        //     'genre'                 => 'Thriller',
        //     'length'                => 122,
        //     'has_watched'           => false,
        //     'director'              => 'Todd Phillips'
        // ]);
        // Movie::create([
        //     'name'                  => 'Friends with benefits',
        //     'year'                  => '2011',
        //     'genre'                 => 'Comedy',
        //     'length'                => 105,
        //     'has_watched'           => true,
        //     'director'              => 'Will GLuck'
        // ]);
        // Movie::create([
        //     'name'                  => 'The Hobbit: An Unexpected Journey',
        //     'year'                  => '2012',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 169,
        //     'has_watched'           => false,
        //     'director'              => 'Peter Jackson'
        // ]);
        // Movie::create([
        //     'name'                  => 'The Hobbit: The Desolation of Smaug',
        //     'year'                  => '2013',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 161,
        //     'has_watched'           => false,
        //     'director'              => 'Peter Jackson'
        // ]);
        // Movie::create([
        //     'name'                  => 'The Hobbit: The Battle of the Five Armies',
        //     'year'                  => '2014',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 144,
        //     'has_watched'           => false,
        //     'director'              => 'Peter Jackson'
        // ]);
        // Movie::create([
        //     'name'                  => "Harry Potter and the Sorcerer's Stone",
        //     'year'                  => '2001',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 152,
        //     'has_watched'           => true,
        //     'director'              => 'Chris Columbus'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Chamber of Secrets',
        //     'year'                  => '2002',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 161,
        //     'has_watched'           => false,
        //     'director'              => 'Chris Columbuse'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Prisoner of Azkaban',
        //     'year'                  => '2004',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 141,
        //     'has_watched'           => true,
        //     'director'              => 'Alfonso Cuarón'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Goblet of Fire',
        //     'year'                  => '2005',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 157,
        //     'has_watched'           => true,
        //     'director'              => 'Mike Newell'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Order of the Phoenix',
        //     'year'                  => '2007',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 138,
        //     'has_watched'           => true,
        //     'director'              => 'David Yates'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Half-Blood Prince',
        //     'year'                  => '2009',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 147,
        //     'has_watched'           => true,
        //     'director'              => 'David Yates'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Deathly Hallows: Part 1',
        //     'year'                  => '2010',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 140,
        //     'has_watched'           => true,
        //     'director'              => 'David Yates'
        // ]);
        // Movie::create([
        //     'name'                  => 'Harry Potter and the Deathly Hallows: Part 2',
        //     'year'                  => '2011',
        //     'genre'                 => 'Fantasy',
        //     'length'                => 125,
        //     'has_watched'           => false,
        //     'director'              => 'David Yates'
        // ]);
    }
}
