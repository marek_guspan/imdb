<?php namespace MarekGuspan\Movies\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMoviesTable extends Migration
{
    public function up()
    {
        Schema::create('marekguspan_movies_movies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('year');
            $table->text('description');
            $table->text('img');
            $table->integer('length');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('marekguspan_movies_movies');
    }
}
