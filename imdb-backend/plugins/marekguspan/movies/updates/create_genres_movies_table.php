<?php namespace MarekGuspan\Movies\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGenresMoviesTable extends Migration
{
    public function up()
    {
        Schema::create('marekguspan_movies_genres_movies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('genre_id');
            $table->integer('movie_id');
            $table->primary(['genre_id', 'movie_id']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('marekguspan_movies_genres_movies');
    }
}
