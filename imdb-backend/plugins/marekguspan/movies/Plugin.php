<?php namespace MarekGuspan\Movies;

use Backend;
use System\Classes\PluginBase;

/**
 * Movies Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Movies',
            'description' => 'No description provided yet...',
            'author'      => 'MarekGuspan',
            'icon'        => 'icon-film'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'MarekGuspan\Movies\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'marekguspan.movies.some_permission' => [
                'tab' => 'Movies',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'movies' => [
                'label'       => 'Movies',
                'url'         => Backend::url('marekguspan/movies/movies'),
                'icon'        => 'icon-film',
                'permissions' => ['marekguspan.movies.*'],
                'order'       => 500,
                'sideMenu' => [
                    'movies' => [
                        'label' => 'movies',
                        'icon'  => 'icon-film',
                        'url'   => Backend::url('marekguspan/movies/movies')
                    ],
                    'genres' => [
                        'label' => 'genres',
                        'icon'  => 'icon-book',
                        'url'   => Backend::url('marekguspan/movies/genres')
                    ],
                    'rating' => [
                        'label' => 'rating',
                        'icon'  => 'icon-star',
                        'url'   => Backend::url('marekguspan/movies/rating')
                    ]
                ]
            ],
        ];
    }
}
