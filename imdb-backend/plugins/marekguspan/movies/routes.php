<?php

use MarekGuspan\Movies\Models\Movie;
use MarekGuspan\Movies\Models\Rating;

$auth = '\Tymon\JWTAuth\Middleware\GetUserFromToken';

//-------RETURN ALL MOVIES-------
Route::GET('/get-all-movies', function () {
    return Movie::all();
});

//-------RETURN MOVIE-------
Route::GET('/get-movie/{id}', function ($id) {
    return Movie::where('id', $id)->first();
});

//-------CREATE MOVIE-------
Route::POST('/create-movie', function () {
    $movie = new Movie();

    $movie->name = Input::get('name');
    $movie->year = Input::get('year');
    $movie->genre = Input::get('genre');
    $movie->length = Input::get('length');
    $movie->description = Input::get('description');

    $movie->save();
});

//-------UPDATE MOVIE-------
Route::PATCH('update-movie/{id}', function ($id) {
    $movie = Movie::where('id', $id)->first();

    $movie->update([
        'name' => Input::get('name', $movie->name),
        'year' => Input::get('year', $movie->year),
        'genre' => Input::get('genre', $movie->genre),
        'length' => Input::get('length', $movie->length),
        'description' => Input::get('description', $movie->description)
    ]);
});

//-------DELETE MOVIE-------
Route::DELETE('delete-movie/{id}', function ($id) {
    $movie = Movie::where('id', $id)->first();
    $movie->delete();
});

//-------ADD RATING-------
Route::POST('/add-rating', function () {
    $rating = new Rating();

    $rating->movie_id = Input::get('movie_id');
    $rating->rating = Input::get('rating');

    $rating->save();
})->middleware($auth);

//-------GET RATINGS FOR SPECIFIC MOVIE--------
Route::GET('/get-ratings/{id}', function ($id) {
    $table = Movie::find($id)->ratings;
    foreach ($table as $row) {
        $ratings[] = $row->rating;
    }
    return $ratings;
});