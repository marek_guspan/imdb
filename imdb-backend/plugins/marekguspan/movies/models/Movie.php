<?php namespace MarekGuspan\Movies\Models;

use Model;

/**
 * Movie Model
 */
class Movie extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'marekguspan_movies_movies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'year', 'genre', 'lentgh', 'has_watched', 'director'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'ratings' => 'MarekGuspan\Movies\Models\Rating'
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'genre' => 'MarekGuspan\Movies\Models\Genre'
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
