<?php namespace MarekGuspan\Movies\Models;

use Model;

/**
 * Genre Model
 */
class Genre extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'marekguspan_movies_genres';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'movie' => 'MarekGuspan\Movies\Models\Movie'
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
