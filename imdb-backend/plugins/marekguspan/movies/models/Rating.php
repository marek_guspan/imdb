<?php namespace MarekGuspan\Movies\Models;

use Model;

/**
 * Rating Model
 */
class Rating extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'marekguspan_movies_ratings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'movie' => 'MarekGuspan\Movie\Models\Movie'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
