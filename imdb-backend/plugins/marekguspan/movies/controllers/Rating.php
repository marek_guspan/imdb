<?php namespace MarekGuspan\Movies\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Rating Back-end Controller
 */
class Rating extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('MarekGuspan.Movies', 'movies', 'rating');
    }
}
