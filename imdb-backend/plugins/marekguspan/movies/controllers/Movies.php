<?php namespace MarekGuspan\Movies\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Movies Back-end Controller
 */
class Movies extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('MarekGuspan.Movies', 'movies', 'movies');
    }

    public function listExtendQuery($query)
    {
        // return $query->where('year', '<', 2013);
        // return $query->where('length', '>', 120);
        // return $query->where('id', 4);
        // return $query->where('hasWatched', 1);
    }

    public function index()
    {
        $this->addCSS('/plugins/marekguspan/movies/controllers/assets/style.css');
        $this->addJS('/plugins/marekguspan/movies/controllers/assets/script.js');
        $this->asExtension('ListController')->index();
    }
}
