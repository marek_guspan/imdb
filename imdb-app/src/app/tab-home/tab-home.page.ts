import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-tab-home',
  templateUrl: './tab-home.page.html',
  styleUrls: ['./tab-home.page.scss'],
  providers: [],
})
export class TabHomePage implements OnInit {
  movies: any;
  constructor(private movieService: MoviesService, private userService: UsersService) {}
  // moviesCommentSection = this.movies.filter( movie => movie.id && 'KOKOT SI');
  ngOnInit() {
    this.movieService.getAllMovies().subscribe(movie => {
      this.movies = movie as any;
    });
    // console.log(this.moviesCommentSection);
    this.userService.loginUser().subscribe( () => console.log(this.userService.getAuthToken()));
  }
}
