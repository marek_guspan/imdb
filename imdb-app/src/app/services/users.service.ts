import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  URL = 'http://localhost:8000/api/v1/auth/login';
  user = {
    email: 'hh@hh.hh',
    password: 'zecmikokot'
  };
  authToken = '';

  getAuthToken() {
    return this.authToken;
  }
  loginUser() {
    return this.http.post(this.URL, this.user)
                    .pipe(map((response: any) => this.authToken = response.token)
    );
  }
}
