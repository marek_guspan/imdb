import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  URL = 'http://localhost:8000/';

  constructor(private http: HttpClient) { }

  getAllMovies() {
    return this.http.get(this.URL + 'get-all-movies');
  }

  // changeRating(id, rating) {
  //   const changeArray = this.movies.find((movie => movie.id === id));
  //   changeArray.rating = rating;
  // }
  // addComment(event, id) {
  //   // let nieco = this.movies.map(movie => {if (movie.id === id) {return movie.comments.push(event.target[0].value); }});
  //   const changeArray = this.movies.find((movie => movie.id === id));
  //   changeArray.comments.push(event.target[0].value);
  // }

  // isCommentSectionDisplayed(id) {
  //   const actualArray = this.movies.find((movie => movie.id === id));
  //   return actualArray.isCommentSectionDisplayed;
  // }

  // setCommentSectionDisplayed(id) {
  //   const changeArray = this.movies.find((movie => movie.id === id));
  //   changeArray.isCommentSectionDisplayed = !changeArray.isCommentSectionDisplayed;
  // }
}
